INSERT INTO doencas(
    id_faixa_temperatura_favoravel_doenca,
    id_faixa_umidade_favoravel_doenca,
    nome,
    nome_cientifico,
    identificacao,
    sintomas
    )
    VALUES
    (1, 1, "Ferrugem Asiática", "Phakopsora Pachyrhizi", "As lesões em fase inicial são esverdeadas a cinza-esverdeadas, mais escuras que o tecido sadio de 1 a 2 mm de diâmetro. Procurar por urédias nas folhas inferiores com uma lupa. Após a esporulação, em cultivar suscetível: lesões castanha clara; em cultivar resistente: lesões castanha avermelhada; neste caso o controle já é mais difícil. Pode ser confundido com Cercospora. Fungo só se reproduz em plantas vivas.", "Lesões foliares tipo encharcamento, começam acinzentadas e ficam marrons, mais visíveis na parte inferior da folhas, exuda uma massa de esporos. Provoca desfolha. Disseminação pelo vento."),
    (2, 2, "Oídio", "Microsphaera Diffusa", "Camada branca de aspecto cotonoso sobre a planta.", "Camada esbranquiçada ou cinza de micélio e esporos (conídios) pulverulentos que pode cobrir toda a parte aérea da planta ou se apresentar com pequenas áreas arredondadas sobre as folhas. Com o passar do tempo, a coloração branca passa a ser castanho acinzentado. As folhas secam e caem prematuramente. Disseminado pelo vento.");