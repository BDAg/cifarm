INSERT INT estagios_reprodutivos
    (estagio, denominacao, descricao)
    VALUES
    ("R1", "Início do florescimento", "Uma flor aberta em qualquer nó do caule (haste principal)." ),
    ("R2", "Florescimento pleno", "Uma flor aberta em um dos 2 últimos nós do caule com folha completamente desenvolvida."),
    ("R3", "Início da formação da vagem", "Vagem com 5mm de comprimento em um dos 4 últimos nós do caule com folhas completamente desenvolvidas."),
    ("R4", "Vagem completamente desenvolvida", "Vagem com 2cm de comprimento em um dos 4 últimos nós do caule com folhas completamente desenvolvidas."),
    ("R5", "Início do enchimento do grão", "Grãos com 3mm de comprimento em vagens em um dos 4 últimos nós do caule, com folhas completamente desenvolvidas."),
    ("R6", "Grão cheio ou completo", "Vagens contendo grãos verdes preenchendo as cavidades da vagem de um dos 4 últimos nós do caule, com folhas completamente desenvolvidas."),
    ("R7", "Início da maturação", "Uma vagem normal no caule com coloração de madura."),
    ("R8", "Maturação plena", "95% das vagens com coloração de madura");
