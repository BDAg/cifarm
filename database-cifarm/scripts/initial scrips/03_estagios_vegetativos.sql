INSERT INTO estagios_vegetativos
    (estagio, denominacao, descricao)
    VALUES
    ("VE", "Emergência", "Cotilédone acima da superfície do solo."),
    ("VC", "Cotilédone", "Cotilédone completamente abertos."),
    ("V1", "Primeiro nó", "Folhas unifolioladas completamente desenvolvidas."),
    ("V2", "Segundo nó", "Primeira folha trifoliolada completamente desenvolvida."),
    ("V3", "Terceiro nó", "Segunda folha trifoliolada completamente desenvolvida."),
    ("V4", "Quarto nó", "Terceira folha trifoliolada completamente desenvolvida."),
    ("V5", "Quinto nó", "Quarta folha trifoliolada completamente desenvolvida."),
    ("V6", "Sexto nó", "Quinta folha trifoliolada completamente desenvolvida.");
