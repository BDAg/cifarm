const express = require('express');
const router = express.Router();
const doencasController = require('../controllers/doencas-controller');
const authMiddleware = require('../middlewares/auth-middleware');

router.post('/cadastro', authMiddleware.authorization, doencasController.postDoencas);
router.get('/', authMiddleware.authorization, doencasController.getDoencas);
router.get('/logs_doencas', doencasController.getLogsDoencas);

module.exports = router;