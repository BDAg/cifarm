const express = require('express');
const router = express.Router();

const pragasController = require('../controllers/pragas-controller');
const doencasController = require('../controllers/doencas-controller');
const culturasController = require("../controllers/culturas-controller");
const authMiddleware = require('../middlewares/auth-middleware');

router.get('/pragas', authMiddleware.authorization,pragasController.getPragas);
router.get('/doencas', authMiddleware.authorization,doencasController.getDoencas);
router.get('/culturas', authMiddleware.authorization, culturasController.getCulturas);

module.exports = router;