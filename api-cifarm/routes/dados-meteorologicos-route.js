const express = require('express');
const router = express.Router();
const dados_meteorologicosController = require('../controllers/dados-meteorologicos-controller');
const authMiddleware = require('../middlewares/auth-middleware');

router.get('/ultimo-registro', dados_meteorologicosController.getUltimoDadoMeteorologico);
router.get('/temperatura', dados_meteorologicosController.getTemperatura);
router.get('/umidade', dados_meteorologicosController.getUmidade);
router.get('/velocidade-vento-km', dados_meteorologicosController.getVelocidadeVentoKM);
router.get('/velocidade-vento-ms', dados_meteorologicosController.getVelocidadeVentoMS);
router.get('/rpm', dados_meteorologicosController.getRPM);
router.get('/pressao-atmosferica', dados_meteorologicosController.getPessaoAtmosferica);
router.get('/', dados_meteorologicosController.getDadosMeteorologicos);
router.post('/', authMiddleware.authorization, dados_meteorologicosController.postDadosMeteorologicos);

module.exports = router;
