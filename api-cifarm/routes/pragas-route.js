const express = require('express');
const router = express.Router();
const pragasController = require('../controllers/pragas-controller');
const authMiddleware = require('../middlewares/auth-middleware');

router.post('/cadastro', authMiddleware.authorization, pragasController.postPragas);
router.get('/', authMiddleware.authorization, pragasController.getPragas);
router.get('/logs_pragas', pragasController.getLogsPragas);

module.exports = router;