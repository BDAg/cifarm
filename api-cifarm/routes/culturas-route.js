const express = require('express');
const router = express.Router();
const culturasController = require('../controllers/culturas-controller');
const authMiddleware = require('../middlewares/auth-middleware');

router.get('/', authMiddleware.authorization, culturasController.getCulturas);
router.post('/cadastro', authMiddleware.authorization, culturasController.postCulturas);

module.exports = router;