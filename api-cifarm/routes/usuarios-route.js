const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarios-controller');
const authMiddleware = require('../middlewares/auth-middleware');


router.get('/', authMiddleware.authorization, usuarioController.getUsuarios);
router.get('/:id_usuario', authMiddleware.authorization, usuarioController.getUsuario);
router.post('/cadastro', authMiddleware.authorization, usuarioController.postUsuario);
router.patch('/:id_usuario',authMiddleware.authorization, usuarioController.patchUsuario);
router.delete('/:id_usuario', authMiddleware.authorization, usuarioController.deleteUsuario);

module.exports = router;