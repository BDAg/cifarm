const mysql = require('../mysql');

exports.authorization = (req, res, next ) => {
    mysql.getConnection((error,conn) => {
        conn.query(
            `SELECT *
                FROM usuarios 
                    WHERE id_usuario = ? and token = ?;
            `,
            [
                req.body._id_user,
                req.body._token
            ]
            ,
            (error, result, field) =>{
                conn.release();
                if(error){
                    return res.status(500).json({
                        error: error
                    });                
                }else {
                    if (result.length > 0) {
                        next();
                    } else {
                        res.json({
                            mensagem: "Usuário não Autenticado"
                        });
                    }
                    
                }
            }
        ); 
    });
}