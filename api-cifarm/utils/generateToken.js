exports.generateToken = (length) => {
    let token = '';
    do {
        token += Math.random().toString(36).substr(2);
    }while (token.length < length) {
        token = token.substr(0, length);
        return token;               
    }    
}