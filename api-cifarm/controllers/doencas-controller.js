const mysql = require('../mysql');

exports.getDoencas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT *
                FROM
                doencas;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        doencas : results
                    });
                }
            }
        );
    }) ;
}


exports.postDoencas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT 
                INTO 
                doencas(
                    nome,
                    nome_cientifico, 
                    descricao
                )
                VALUES
                    (?, ?, ?);`,
                [
                    req.body.nome,                
                    req.body.nome_cientifico,            
                    req.body.descricao        
                ],
                (error, results, fields) => {
                    conn.release();
                    if(error){
                        res.status(500).json({
                            error : error,
                            mensagem : "Não foi possível realizar a operação"
                        });
                    }else{
                        res.status(201).json({
                            mensagem : "Doença inserida com sucesso!",
                            id : results.isertedId
                        });
                    }
                }
        );
    });
}

exports.getLogsDoencas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `CALL log_doencas(1);`,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        logs_doencas : results
                    });
                }
            }
        );
    }) ;
}