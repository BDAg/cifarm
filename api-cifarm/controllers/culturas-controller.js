const mysql = require('../mysql');

exports.getCulturas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT * 
                FROM
                culturas;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        culturas: results
                    });
                }
            }
        );
    })
}

exports.postCulturas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT INTO
                culturas(
                    nome,
                    nome_cientifico,
                    descricao
                )
                VALUES (?,?,?);
            `,
            [
                req.body.nome,
                req.body.nome_cientifico,
                req.body.descricao
            ],
            (error, results, filds) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                }else{
                    return res.status(201).json({
                        mensagem : "Cultura inserida com sucesso!",
                            id : results.isertedId
                    });
                }

            }
        );
    });
}