const mysql = require('../mysql');

exports.getPragas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT * 
                FROM
                pragas;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        pragas: results
                    });
                }
            }
        );
    })
}

exports.postPragas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT 
                INTO 
                pragas(
                    nome,
                    nome_cientifico, 
                    descricao
                )
                VALUES
                    (?, ?, ?);`,
                [
                    req.body.nome,                
                    req.body.nome_cientifico,
                    req.body.descricao        
                ]
    
                ,
                (error, results, fields) => {
                    conn.release();
                    if(error){
                        res.status(500).json({
                            error : error,
                            mensagem : "Não foi possível realizar a operação"
                        });
                    }else{
                        res.status(201).json({
                            mensagem : "Praga inserida com sucesso!",
                            id : results.isertedId
                        });
                    }
                }
        );
    });
}

exports.getLogsPragas = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `CALL log_pragas(1);`,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        logs_pragas: results
                    });
                }
            }
        );
    })
}