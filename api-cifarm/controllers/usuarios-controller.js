const mysql = require('../mysql');
const token = require('./../utils/generateToken');


exports.getUsuarios = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT *
                FROM
                usuarios;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        usuarios : results
                    });
            }
        }
        );
    });
}

exports.getUsuario = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT * 
                FROM usuarios
                    WHERE id_usuario = ?;`,
            [
                req.params.id_usuario
            ],
            (error, result, fields) => {
                conn.release();
                if(error)
                    return res.status(500).send({error: error});
                
                    return res.status(200).send({
                        usuario : result
                    });
                
            }
        );        
    });
}
exports.postUsuario = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT 
                INTO 
                usuarios(
                    id_tipo_usuario,
                    nome, 
                    email,
                    cpf, 
                    token
                )
                VALUES
                    (?, ?, ?, ?, ?);`,
                [
                    req.body.id_tipo_usuario,                
                    req.body.nome,            
                    req.body.email,
                    req.body.cpf,
                    req.body.token = token.generateToken(45)      
                ],
                (error, results, fields) => {
                    conn.release();
                    if(error){
                        res.status(500).json({
                            error : error,
                            mensagem : "Não foi possível realizar a operação"
                        });
                    }else{
                        res.status(201).json({
                            mensagem : "Usuário inserido com sucesso!",
                            id : results.isertedId
                        });
                    }
                }
        );
    });
}

exports.patchUsuario = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `UPDATE 
                usuarios
                
                SET
                    nome = ?,
                    email = ?,
                    cpf =  ?
                WHERE
                    id_usuario = ?;`,
                    [
                        req.body.nome,
                        req.body.email,
                        req.body.cpf,
                        req.params.id_usuario   
                    ],
                (error, results, fields) => {
                    conn.release();
                    if(error){
                        res.status(500).json({
                            error : error,
                            mensagem : "Não foi possível realizar a operação"
                        });
                    }else{
                        res.status(201).json({
                            mensagem : "Usuário modificado com sucesso!",
                            id : results.isertedId
                        });
                    }
                }
        );  
    });
}

exports.deleteUsuario = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `DELETE FROM
                usuarios
            WHERE
                id_usuario = ?`,
                [
                    req.params.id_usuario
                ],
                (error, results, fields) => {
                    conn.release();
                    if(error){
                        res.status(500).json({
                            error : error,
                            mensagem : "Não foi possível realizar a operação"
                        });
                    }else{
                        res.status(201).json({
                            mensagem : "Usuário apagado com sucesso!",
                            id : results.isertedId
                        });
                    }
                }

        );
    });
}