const mysql = require("../mysql");

exports.getTemperatura = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                temperatura, 
                created
            FROM
                dados_meteorologicos
            ORDER BY 
                id_dado_meteorologico 
                    DESC 
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        temperatura: results
                    });
                }
            }
        );
    });
}

exports.getUmidade = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                umidade,
                created
            FROM
                dados_meteorologicos
            ORDER BY
                id_dado_meteorologico
                    DESC
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        umidade: results
                    });
                }
            }
        );
    });
}

exports.getVelocidadeVentoKM = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                velocidade_vento_km,
                created
            FROM
                dados_meteorologicos
            ORDER BY
                id_dado_meteorologico
                    DESC
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        velocidade_vento_km : results
                    });
                }
            }
        );
    });
}

exports.getVelocidadeVentoMS = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                velocidade_vento_ms,
            created
            FROM
                dados_meteorologicos
            ORDER BY
                id_dado_meteorologico
                    DESC
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        velocidade_vento_ms: results
                    });
                }
            }
        );
    });
}

exports.getRPM = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                rpm,
                created
            FROM
                dados_meteorologicos
            ORDER BY
                id_dado_meteorologico
                    DESC
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        rpm: results
                    });
                }
            }
        );
    });
}

exports.getPessaoAtmosferica = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT
                pressao_atmosferica,
                created
            FROM
                dados_meteorologicos
            ORDER BY
                id_dado_meteorologico
                    DESC
            LIMIT 6;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        pressao_atmosferica: results    
                    });
                }
            }
        );
    });
}

exports.getDadosMeteorologicos = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `SELECT 
                dados_meteorologicos.id_estacao_meteorologica,
                direcoes.direcao_vento,
                dados_meteorologicos.temperatura,
                dados_meteorologicos.umidade,
                velocidade_vento_km,
                velocidade_vento_ms,
                pressao_atmosferica,
                rpm,
                created
            FROM dados_meteorologicos
                INNER JOIN direcoes
                    ON direcoes.id_direcao_vento = dados_meteorologicos.id_direcao_vento;
            `,
            (error, results, fields) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        dados_meteorologicos: results
                    });
                }
            }
        );
    });
}

exports.getUltimoDadoMeteorologico = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `
            SELECT 
	            dados_meteorologicos.id_estacao_meteorologica,
                direcoes.direcao_vento,
                dados_meteorologicos.temperatura,
                dados_meteorologicos.umidade,
                velocidade_vento_km,
                velocidade_vento_ms,
                pressao_atmosferica,
                rpm,
                created
            FROM dados_meteorologicos
            	INNER JOIN direcoes
            		ON direcoes.id_direcao_vento = dados_meteorologicos.id_direcao_vento
            	WHERE id_dado_meteorologico = (SELECT max(id_dado_meteorologico) FROM dados_meteorologicos);
            `,
            (error, result, field) => {
                conn.release();
                if (error) {
                    return res.status(500).json({
                        error: error
                    });
                } else {
                    return res.status(200).json({
                        dado_meteorologico: result[0]
                    });
                }
            }
        );
    });
}

exports.postDadosMeteorologicos = (req, res, next) => {
    mysql.getConnection((error, conn) => {
        conn.query(
            `INSERT INTO dados_meteorologicos
                (
                    id_estacao_meteorologica,
                    id_direcao_vento,
                    temperatura,
                    umidade,
                    velocidade_vento_km,
                    velocidade_vento_ms,
                    rpm,
                    pressao_atmosferica

                )
                    VALUES
                        (?, ?, ?, ?, ?, ?, ?, ?);
            `,
            [
                req.body.id_estacao_meteorologica,
                req.body.id_direcao_vento,
                req.body.temperatura,
                req.body.umidade,
                req.body.velocidade_vento_km,
                req.body.velocidade_vento_ms,
                req.body.rpm,
                req.body.pressao_atmosferica
            ],
            (error, result, field) => {
                conn.release();
                if (error) {
                    res.status(500).json({
                        error: error,
                        mensagem: "Não foi possível realizar a operação"
                    });
                } else {
                    res.status(201).json({
                        mensagem: "Dado meteorológico inserido com sucesso!",
                        id: result.isertedId
                    });
                }
            }
        );
    });
}