//***************************CIFARM*******************************//
//Projeto: Estação Meteorológica                                  //
//Data : 25/08/2019                                               //
//Versão: 1.1                                                     //
//****************************************************************//

//************Links das Bibliotecas Utilizadas**********************//
//https://www.arduinoecia.com.br/sensor-de-umidade-e-temperatura-dht11/
//                                                                  //
//********************************************************************

//*********************Esquema de Ligação****************************//
//Arduino Uno                        ESTAÇÃO                         //
// D2 ---------------------------->FIO VERMELHO                      //
// A0----------------------------->FIO VERDE                         //
// 5V----------------------------->FIO AMARELO                       //
// GND---------------------------->FIO PRETO                         //
//********************************************************************



//------------------- definição das constantes-------------------//
int pin=A0;
#include <SD.h>
#define Hall sensor 2               // Pino digital 2
#include <dht.h>                    // inclui biblioteca dht11
#define dht_dpin 9                  // digital 9 dht 
int portaLDR = A2;                  // LDR porta analogica A2
const float pi = 3.14159265;        // Numero pi
int period = 5000;                  // Tempo de medida(miliseconds)
int delaytime = 2000;               // Time between samples (miliseconds)
int radius = 147;                   // Raio do anemometro(mm)
String dir;                         //  dir e uma string
//----------------------------------------------------------------//


//----------------Definição das Variaveis Globais-----------------//
unsigned int Sample = 0;             // Sample number
unsigned int counter = 0;            // magnet counter for sensor
unsigned int RPM = 0;                // Revolutions per minute
float speedwind = 0;                 // Wind speed (m/s)
float windspeed = 0;                 // Wind speed (km/h)
float tempoinicio ;
float tempofim;
float valor =0;
int windDir =0;
File sensores;                       // Variavel do Arquivo txt 
//-----------------------------------------------------------------//



void setup() {
 
  pinMode(2, INPUT);
  digitalWrite(2, HIGH);     //internall pull-up active
  Serial.begin(9600); 
  Serial.print("Inicializando cartão SD...");
  //inicializacao do cartão SD

  if (!SD.begin(4)) 
  {
  Serial.println("Inicializacao do cartão SD falhou!");
  return;
  }
  Serial.println("Inicializacao do cartão SD concluida");
}

void loop() {

 valor = analogRead(pin)* (5.0 / 1023.0);
 int estado = analogRead(portaLDR);  //Lê o valor fornecido pelo LDR  
 dht DHT; //Inicializa o sensor
 DHT.read11(dht_dpin); 

  if (valor <= 0.27) {
  dir = "NO";
  windDir = 315 ;
  }

  else if (valor <= 0.32) { 
  dir = "O";
  windDir = 270;
  }
  else if (valor <= 0.38) {
  dir = "SU"; 
  windDir = 225;
  }
  else if (valor <= 0.45) {
  dir = "S"; 
  windDir = 180;
  }
  else if (valor <= 0.57) { 
  dir = "SE";
  windDir = 135;
  }
  else if (valor <= 0.75) { 
  dir = "E";
  windDir = 90;
  }
  else if (valor <= 1.25) { 
  dir = "NE";
  windDir = 45;
  }
  else {  
  dir = "N";
  windDir = 0;
  }
  //****************************************************************
  Serial.print("Leitura de tensão da Biruta:");
  Serial.print(valor);
  Serial.println(" volt");
  Serial.print("Direcao do vento a:");
  Serial.print(windDir);
  Serial.println(" graus");
  Serial.print("Sentido:" );
  Serial.println(dir);
  //*****************************************************************
  Serial.print("Umidade = "); 
  Serial.print(DHT.humidity); 
  Serial.print(" %  ");
  Serial.print("Temperatura = "); 
  Serial.print(DHT.temperature); 
  Serial.print(" Celsius  "); 
  Serial.print("Luminosidade: ");
  Serial.println(estado);

  //*****************************************************************
  Sample++;
  Serial.print(Sample);
  Serial.print(": Coletando...");
  windvelocity();
  Serial.print(" finished.");
  Serial.print("Counter: ");
  Serial.println(counter);
  Serial.print(";  RPM: ");
  RPMcalc();
  Serial.print(RPM);
  Serial.print(";  Wind speed: ");
 
  //*****************************************************************
  //print m/s  
  WindSpeed();
  Serial.print(windspeed);
  Serial.print(" [m/s] ");              
  
  //*****************************************************************
  //print km/h  
  SpeedWind();
  Serial.print(speedwind);
  Serial.print(" [km/h] ");  
  Serial.println();
  delay(delaytime);                        //delay between prints
 
  gravaSensor(); // coleta os dados dos sensores e insere no cartao SD
}


// Measure wind speed
void windvelocity(){
  speedwind = 0;
  windspeed = 0;
  
  counter = 0;  
  attachInterrupt(0, addcount, RISING);
  unsigned long millis();       
  long startTime = millis();
  while(millis() < startTime + period) {
  }
}


void RPMcalc(){
  RPM=((counter)*60)/(period/1000);  // Calculate revolutions per minute (RPM)
}

void WindSpeed(){
  windspeed = ((4 * pi * radius * RPM)/60) / 1000;  // Calculate wind speed on m/s
 
}

void SpeedWind(){
  speedwind = (((4 * pi * radius * RPM)/60) / 1000)*3.6;  // Calculate wind speed on km/h
 
}

void addcount(){
  counter++;
} 

void gravaSensor(){
  sensores = SD.open("dados.txt", FILE_WRITE);

 
  if (sensores) {
   
  sensores.print("(");
  sensores.print(dir); //direcao do vento
  sensores.print(", ");
  sensores.print(DHT.humidity); // umidade sensor DHT
  sensores.print(", ");
  sensores.print(DHT.temperature); // temperatura sensor DHT
  sensores.print(", ");
  sensores.print(estado); // leitura do LDR
  sensores.print(", ");
  sensores.print(speedwind); // velocidade do vento    
  sensores.println("),");




    
  sensores.close();
  Serial.println("concluido.");
  } else {
    Serial.println("erro ao fechar dados.txt");
  }
}








 
