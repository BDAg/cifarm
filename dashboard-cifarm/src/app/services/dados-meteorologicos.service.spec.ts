import { TestBed } from '@angular/core/testing';

import { DadosMeteorologicosService } from './dados-meteorologicos.service';

describe('DadosMeteorologicosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DadosMeteorologicosService = TestBed.get(DadosMeteorologicosService);
    expect(service).toBeTruthy();
  });
});
