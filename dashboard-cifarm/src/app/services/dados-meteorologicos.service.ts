import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from './utils.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DadosMeteorologicosService {

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  public getUltimoDadoMeteorologico() {
    return this.http.get(`${this.utils.API_URL}/dados-meteorologicos/ultimo-registro`);
  }

  public getDadosMeteorologicos() {
    return this.http.get(`${this.utils.API_URL}/dados-meteorologicos/`);
  }

  public getTemperatura() {
    return this.http.get(`${this.utils.API_URL}/dados-meteorologicos/temperatura`);
  }

  public getUmidade() {
    return this.http.get<[]>(`${this.utils.API_URL}/dados-meteorologicos/umidade`);
  }

  public getVelocidadeVentoKm() {
    return this.http.get<[]>(`${this.utils.API_URL}/dados-meteorologicos/velocidade-vento-km`);
  }

  public getVelocidadeVentoMs() {
    return this.http.get<[]>(`${this.utils.API_URL}/dados-meteorologicos/velocidade-vento-ms`);
  }

  public getPressaoAtmosferica() {
    return this.http.get<[]>(`${this.utils.API_URL}/dados-meteorologicos/pressao-atmosferica`);
  }
}
