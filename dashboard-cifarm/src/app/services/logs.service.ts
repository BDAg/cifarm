import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(
    private http: HttpClient,
    private utils: UtilsService
  ) { }

  public getLogsPragas() {
    return this.http.get<[]>(`${this.utils.API_URL}/pragas/logs_pragas`);
  }

  public getLogsDoencas() {
    return this.http.get<[]>(`${this.utils.API_URL}/doencas/logs_doencas`);
  }
}
