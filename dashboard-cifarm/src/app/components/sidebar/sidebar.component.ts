import { Component, OnInit } from "@angular/core";

declare interface RouteInfo {
  path: string;
  title: string;
  rtlTitle: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/temporeal",
    title: "Estação Meteorológica",
    rtlTitle: "لوحة القيادة",
    icon: "./../../../assets/icons/thermometer.svg",
    class: ""
  },
  {
    path: "/dadosmeteorologicos",
    title: "Registros",
    rtlTitle: "لوحة القيادة",
    icon: "/../../../assets/img/folder.svg",
    class: ""
  },
  {
    path: "/dashboard",
    title: "Gráficos",
    rtlTitle: "لوحة القيادة",
    icon: "/../../../assets/icons/chart.svg",
    class: ""
  },
  {
    path: "/logs",
    title: "Logs",
    rtlTitle: "قائمة الجدول",
    icon: "./../../../assets/icons/warning.svg",
    class: ""
  },
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
