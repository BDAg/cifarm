import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { IconsComponent } from "../../pages/icons/icons.component";
import { MapComponent } from "../../pages/map/map.component";
import { NotificationsComponent } from "../../pages/notifications/notifications.component";
import { UserComponent } from "../../pages/user/user.component";
import { TablesComponent } from "../../pages/tables/tables.component";
import { DadosmeteorologicosComponent } from '../../pages/dadosmeteorologicos/dadosmeteorologicos.component';
import { DadostabeladoComponent } from '../../pages/dadostabelado/dadostabelado.component';
//import { TypographyComponent } from "../../pages/typography/typography.component";
// import { RtlComponent } from "../../pages/rtl/rtl.component";
import { LogsComponent } from '../../pages/logs/logs.component';

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  {path:"temporeal", component: DadosmeteorologicosComponent},
  {path:"dadosmeteorologicos", component: DadostabeladoComponent},
  {path:"logs", component: LogsComponent},
  //{ path: "icons", component: IconsComponent },
  { path: "maps", component: MapComponent },
  { path: "notifications", component: NotificationsComponent },
  { path: "user", component: UserComponent },
  { path: "tables", component: TablesComponent },
  //{ path: "typography", component: TypographyComponent },
  // { path: "rtl", component: RtlComponent }
];
