import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DadosmeteorologicosComponent } from './dadosmeteorologicos.component';

@NgModule({
  declarations: [DadosmeteorologicosComponent],
  imports: [
    CommonModule
  ],
  exports:[
    DadosmeteorologicosComponent
  ]
})
export class DadosmeteorologicosModule { }
