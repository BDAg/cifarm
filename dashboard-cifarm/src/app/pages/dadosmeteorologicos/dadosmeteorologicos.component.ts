import { Component, OnInit } from '@angular/core';
import { DadosMeteorologicosService } from 'src/app/services/dados-meteorologicos.service';



@Component({
  selector: 'app-dadosmeteorologicos',
  templateUrl: './dadosmeteorologicos.component.html',
  styleUrls: ['./dadosmeteorologicos.component.scss']
})
export class DadosmeteorologicosComponent implements OnInit {

  dadoMeteorologico: any;

  constructor(private dadosMeteorologicosService: DadosMeteorologicosService) { }

  ngOnInit() {
    this.getUltimoDadoMeteorologico();
  }

  public getUltimoDadoMeteorologico() {
    this.dadosMeteorologicosService.getUltimoDadoMeteorologico()
      .subscribe(data => {
        this.dadoMeteorologico = data['dado_meteorologico'];
        console.log(this.dadoMeteorologico);
      });


  }

}
