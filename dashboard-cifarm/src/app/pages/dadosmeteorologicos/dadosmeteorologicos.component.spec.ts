import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosmeteorologicosComponent } from './dadosmeteorologicos.component';

describe('DadosmeteorologicosComponent', () => {
  let component: DadosmeteorologicosComponent;
  let fixture: ComponentFixture<DadosmeteorologicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosmeteorologicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosmeteorologicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
