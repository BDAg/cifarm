import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DadostabeladoComponent } from './dadostabelado.component';

@NgModule({
  declarations: [DadostabeladoComponent],
  imports: [
    CommonModule
  ],
  exports: [
    DadostabeladoComponent
  ]
})
export class DadostabeladoModule { }
