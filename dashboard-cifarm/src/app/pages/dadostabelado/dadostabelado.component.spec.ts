import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadostabeladoComponent } from './dadostabelado.component';

describe('DadostabeladoComponent', () => {
  let component: DadostabeladoComponent;
  let fixture: ComponentFixture<DadostabeladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadostabeladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadostabeladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
