import { Component, OnInit } from '@angular/core';
import { DadosMeteorologicosService } from 'src/app/services/dados-meteorologicos.service';

@Component({
  selector: 'app-dadostabelado',
  templateUrl: './dadostabelado.component.html',
  styleUrls: ['./dadostabelado.component.scss']
})
export class DadostabeladoComponent implements OnInit {

  dadosMeteorologicos: any;

  constructor(private dadosMeteorologicosService: DadosMeteorologicosService) { }

  ngOnInit() {
    this.getUltimoDadoMeteorologico();
  }

  public getUltimoDadoMeteorologico() {
    this.dadosMeteorologicosService.getDadosMeteorologicos()
      .subscribe(data => {
        this.dadosMeteorologicos = data['dados_meteorologicos'];
        console.log(this.dadosMeteorologicos);
      });


  }

}
