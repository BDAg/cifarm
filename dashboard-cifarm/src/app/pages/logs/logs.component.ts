import { Component, OnInit } from '@angular/core';
import { LogsService } from 'src/app/services/logs.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
  logs_pragas: any;
  logs_doencas: any;

  constructor(
    private logsService: LogsService
  ) { }

  ngOnInit() {
    this.logsService.getLogsDoencas()
    .subscribe((data) => {
      this.logs_doencas = data['logs_doencas'];
      console.log(this.logs_doencas);
    });

    this.logsService.getLogsPragas()
    .subscribe((data) => {
      this.logs_pragas = data['logs_pragas'];
      console.log(this.logs_pragas);
    });
  }


}
